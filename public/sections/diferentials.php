<section id="diferentials">
    <div class="col-12 col-xl-10 hazy-bg pb-5">
        <div class="container pl-xl-5 pr-xl-0">
            <div class="row">
                <div class="col-12 col-xl-7 p-0 order-2 order-xl-1">                
                    <div class="row ml-xl-5 pl-xl-5 text-uppercase">
                        <div class="text-box pt-5 pb-5 pl-0 col-12 text-left">
                            <h1 class="text-uppercase"><strong>Diferenciais</strong></h1>
                        </div>
                        <div class="container">
                            <div class="row mb-3 d-flex align-items-center">
                                <div class="col-3">
                                    <img src="images/dif-1.png">
                                </div>
                                <div class="col-9">
                                    <p>Atendimento personalizado e humanizado, com um médico de referência, que conhece você e todo seu histórico de saúde.</p>
                                </div>
                            </div>
                        
                            <div class="row mb-3 d-flex align-items-center">
                                <div class="col-3">
                                    <img src="images/dif-2.png">
                                </div>
                                <div class="col-9">
                                    <p>Histórico de saúde eletrônico:</p>
                                    <span>todo os profissionais da Unidade têm acesso ao seu histórico, podendo realizar um atendimento personalizado, mesmo não sendo o seu médico ou equipe de referência.</span>
                                </div>
                            </div>
                            <div class="row mb-3 d-flex align-items-center">
                                <div class="col-3">
                                    <img src="images/dif-3.png">
                                </div>
                                <div class="col-9">
                                    <p>Programas de promoção da saúde.</p>
                                </div>
                            </div>
                            <div class="row mb-3 d-flex align-items-center">
                                <div class="col-3">
                                    <img src="images/dif-4.png">
                                </div>
                                <div class="col-9">
                                    <p>Médico com habilidades técnicas e humanas.</p>
                                </div>
                            </div>
                            <div class="row mb-3 d-flex align-items-center">
                                <div class="col-3">
                                    <img src="images/dif-1.png">
                                </div>
                                <div class="col-9">
                                    <p>Equipe multiprofissional construindo o plano de cuidados.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-xl-5 order-1 order-xl-2 hazy-image d-none d-xl-block pt-4">
                    <img src="images/orange-family.png">
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-center pt-5 mt-5">
        <img class="tri-image" src="images/tri-5.png">
    </div>			
</section>