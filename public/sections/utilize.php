<section id="utilize" class="pt-5">
    <div class="container">
        <div class="row">
            <div class="col-12 ">
                <h1 class="text-uppercase pb-5">Como Utilizar</h1>				
            </div>
            <div id="violin" class="col-12 mb-5">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="card-header border-0" id="how-1">
                                <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseHOne" aria-expanded="false" aria-controls="collapseHOne">
                                    <h5>Como agendo uma consulta com o meu médico de família no Unimed Pleno?</h5>
                                    <span class="btn btn-link">
                                        <img src="/images/adicao.svg" width="15px">
                                    </span>
                                </a>
                            </div>
                            <div id="collapseHOne" class="collapse" aria-labelledby="how-1" data-parent="#violin">
                                <div class="card-body">
                                    <p>Pelo ramal 2588 ou telefone 3220-6273.</p>
                                </div>											
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="card-header border-0" id="how-2">
                                <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseHTwo" aria-expanded="false" aria-controls="collapseHTwo">
                                    <h5>Posso ir diretamente à Unidade de Atenção Primária à Saúde sem hora marcada?</h5>
                                    <span class="btn btn-link">
                                        <img src="/images/adicao.svg" width="15px">
                                    </span>
                                </a>
                            </div>
                            <div id="collapseHTwo" class="collapse" aria-labelledby="how-2" data-parent="#violin">
                                <div class="card-body">
                                    <p class="text-justify">A agenda de cada médico possui horários disponíveis para atendimento sem hora marcada. 
                                        São horários protegidos para esse tipo de atendimento (agenda aberta/encaixe). 
                                        E o cliente sempre será acolhido por um profissional da enfermagem para priorizar o atendimento, 
                                        dependendo da necessidade. Caso seja uma emergência médica, a orientação é o paciente procurar um serviço 
                                        de pronto-atendimento da Rede Pleno. Não é necessário encaminhamento para acessar um pronto-atendimento.</p>
                                </div>											
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="card-header border-0" id="how-3">
                                <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseHThree" aria-expanded="false" aria-controls="collapseHThree">
                                    <h5>Como solicito autorização de um procedimento?</h5>
                                    <span class="btn btn-link">
                                        <img src="/images/adicao.svg" width="15px">
                                    </span>
                                </a>
                            </div>
                            <div id="collapseHThree" class="collapse" aria-labelledby="how-3" data-parent="#violin">
                                <div class="card-body">
                                    <p class="text-justify">De acordo com a natureza do procedimento, poderá ser autorizado na própria unidade, na Central de Atendimento, App ou site.</p>
                                </div>											
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="card-header border-0" id="how-4">
                                <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseHFour" aria-expanded="false" aria-controls="collapseHFour">
                                    <h5>Como faço o agendamento de exames?</h5>
                                    <span class="btn btn-link">
                                        <img src="/images/adicao.svg" width="15px">
                                    </span>
                                </a>
                            </div>
                            <div id="collapseHFour" class="collapse" aria-labelledby="how-4" data-parent="#violin">
                                <div class="card-body">
                                    <p class="text-justify">O agendamento deverá ser feito na recepção da Unidade, onde o devido direcionamento para os locais de referência será feito.</p>
                                </div>											
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="card-header border-0" id="how-5">
                                <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseHFive" aria-expanded="false" aria-controls="collapseHFive">
                                    <h5>Quando vou pagar coparticipação?</h5>
                                    <span class="btn btn-link">
                                        <img src="/images/adicao.svg" width="15px">
                                    </span>
                                </a>
                            </div>
                            <div id="collapseHFive" class="collapse" aria-labelledby="how-5" data-parent="#violin">
                                <div class="card-body">
                                    <p class="text-justify">O pagamento de coparticipação será apenas para procedimentos realizados fora da Unidade de Atenção Primária à Saúde, 
                                        como consultas com especialistas, exames e procedimentos em outros serviços e consultas de urgência e emergência em pronto-socorro. Ou seja, 
                                        as consultas, exames e procedimentos realizados dentro da Unidade não terão cobrança de coparticipação.</p>
                                </div>											
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="card-header border-0" id="how-6">
                                <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseHSix" aria-expanded="false" aria-controls="collapseHSix">
                                    <h5>E se eu precisar de um médico de outra especialidade?</h5>
                                    <span class="btn btn-link">
                                        <img src="/images/adicao.svg" width="15px">
                                    </span>
                                </a>
                            </div>
                            <div id="collapseHSix" class="collapse" aria-labelledby="how-6" data-parent="#violin">
                                <div class="card-body">
                                    <p class="text-justify">Apesar de o Médico de Família ser capacitado para cuidar das pessoas independente da idade, sexo ou tipo de problema que 
                                        elas tenham, existem algumas situações em que é necessária a opinião de um especialista. Nesses casos, o médico da Unidade APS fará o devido 
                                        encaminhamento para a especialidade necessária, aos profissionais de referência integrados à equipe da APS. Após avaliação do profissional da 
                                        especialidade referenciada e estabelecimento da conduta necessária para resolução do problema do paciente, este médico fará a contrarreferência 
                                        para a Unidade APS, possibilitando, assim, a coordenação do cuidado dos usuários vinculados à Atenção Primária à Saúde.</p>
                                </div>											
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="card-header border-0" id="how-7">
                                <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseHSeven" aria-expanded="false" aria-controls="collapseHSeven">
                                    <h5>Como funciona o encaminhamento e o agendamento de consulta com o médico especialista?</h5>
                                    <span class="btn btn-link">
                                        <img src="/images/adicao.svg" width="15px">
                                    </span>
                                </a>
                            </div>
                            <div id="collapseHSeven" class="collapse" aria-labelledby="how-7" data-parent="#violin">
                                <div class="card-body">
                                    <p class="text-justify">Após o atendimento com médico de família, você deverá passar pela recepção para liberação e agendamento da consulta com o 
                                        especialista.</p>
                                </div>											
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="card-header border-0" id="how-8">
                                <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseHEight" aria-expanded="false" aria-controls="collapseHEight">
                                    <h5>E se eu precisar de atendimento fora da minha cidade?</h5>
                                    <span class="btn btn-link">
                                        <img src="/images/adicao.svg" width="15px">
                                    </span>
                                </a>
                            </div>
                            <div id="collapseHEight" class="collapse" aria-labelledby="how-8" data-parent="#violin">
                                <div class="card-body">
                                    <p class="text-justify">Caso o usuário esteja fora da cidade de origem e tiver algum problema caracterizado como urgência e/ou emergência, 
                                        poderá utilizar os serviços de Pronto Atendimento do Sistema Unimed em todo o território nacional. Em caso de atendimentos eletivos, deverá 
                                        aguardar o retorno à cidade de origem para realizar o agendamento conforme a necessidade.</p>
                                </div>											
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="card-header border-0" id="how-9">
                                <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseHNine" aria-expanded="false" aria-controls="collapseHNine">
                                    <h5>Quais hospitais devo procurar em casos de urgência e emergência?</h5>
                                    <span class="btn btn-link">
                                        <img src="/images/adicao.svg" width="15px">
                                    </span>
                                </a>
                            </div>
                            <div id="collapseHNine" class="collapse" aria-labelledby="how-9" data-parent="#violin">
                                <div class="card-body">
                                    <p class="text-justify">Hospital Rio Grande, Hospital Unimed Natal, Policlínica LIGA, Prontoclínica da Criança, Hospital Memorial, Luiz Antônio LIGA, Clínica Santa Maria e Associação Proviver.</p>
                                </div>											
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>