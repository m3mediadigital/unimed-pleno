<section id="video">
    <div class="box-text purple-bg pt-5 pb-5">
        <div class="container pb-3 pb-lg-0">
            <p class="text-center pt-5">
            A Atenção Integral à Saúde é um conceito amplo que reflete uma estratégia frente à necessidade de concepção de um novo modelo para a saúde, tendo como um dos segmentos fortes a Atenção Primária e a ênfase na Medicina Preventiva, alicerçando-se na promoção da saúde e na prevenção de riscos e doenças.
            </p>
        </div>
        <div class="purple-box pb-5">
            <div class="green-box"></div>
        </div>
    </div>
    <!--<iframe src="https://www.youtube.com/embed/u8SB1AJmbQQ" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
    <iframe width="560" height="315" src="https://www.youtube.com/embed/u8SB1AJmbQQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <!-- <div class="row"> -->
        <div class="row d-flex justify-content-center">
            <img class="tri-image" src="images/tri-2.png">
        </div>
        <!-- <div class="relative-box">
            <img class="tri-image" src="images/tri-2.png">
        </div> -->
    <!-- </div> -->
</section>