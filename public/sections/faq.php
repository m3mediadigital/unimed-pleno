<section id="faq">
    <div class="container">
        <div class="row">
            <div class="text-box pt-5 pb-5 col-12 text-left">
                <h1 class="text-uppercase pt-5 pb-5">Dúvidas Frequentes</h1>
            </div>
            <div id="bass" class="col-12">
                <div id="violin" class="col-12 mb-5">
                    <div class="row">
                    
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-1">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH1" aria-expanded="false" aria-controls="collapseH1">
                                        <h5>O que é o Unimed Pleno?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH1" class="collapse" aria-labelledby="how-1" data-parent="#violin">
                                    <div class="card-body">
                                        <p>É um novo modelo de cuidado integral em saúde que tem como objetivo promover a qualidade de vida das pessoas através do acompanhamento contínuo por uma equipe multidisciplinar, 
                                            intervindo nos fatores que colocam a sua saúde em risco e trabalhando em conjunto na prevenção ao aparecimento de novas doenças.</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-2">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH2" aria-expanded="false" aria-controls="collapseH2">
                                        <h5>Como obter acesso aos médicos especialistas?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH2" class="collapse" aria-labelledby="how-2" data-parent="#violin">
                                    <div class="card-body">
                                        <p>O acesso aos médicos especialistas ocorrerá mediante encaminhamento do Médico de Família de referência do usuário.</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-3">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH3" aria-expanded="false" aria-controls="collapseH3">
                                        <h5>Como é a comunicação do médico de referência com o médico especialista?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH3" class="collapse" aria-labelledby="how-3" data-parent="#violin">
                                    <div class="card-body">
                                        <p>A comunicação entre o médico de família de referência do usuário e o médico especialista ocorre por meio de um processo 
                                            coordenado chamado referência e contrarreferência. Isso quer dizer que o médico de família encaminha o paciente, mediante 
                                            instrumento estabelecido em Prontuário Eletrônico compartilhado, ao médico especialista, o qual avalia as informações e 
                                            registra também as informações pertinentes em Prontuário. Caso necessário, os profissionais médicos poderão fazer contato 
                                            por outros meios, como telefone, para melhor interlocução sobre o caso específico do paciente.</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-4">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH4" aria-expanded="false" aria-controls="collapseH4">
                                        <h5>Como saber qual o médico de referência do usuário?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH4" class="collapse" aria-labelledby="how-4" data-parent="#violin">
                                    <div class="card-body">
                                        <p>Basta entrar em contato pelo ramal 2588 ou telefone 3220-6273.</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-5">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH5" aria-expanded="false" aria-controls="collapseH5">
                                        <h5>Estou em processo cirúrgico em andamento, como posso proceder?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH5" class="collapse" aria-labelledby="how-5" data-parent="#violin">
                                    <div class="card-body">
                                        <p>Você pode entrar em contato com a Unidade de Atenção Primária à Saúde para análise e andamento do processo.</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-6">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH6" aria-expanded="false" aria-controls="collapseH6">
                                        <h5>Já tenho autorização para fazer um procedimento no plano antigo, o que devo fazer?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH6" class="collapse" aria-labelledby="how-6" data-parent="#violin">
                                    <div class="card-body">
                                        <p>A sua equipe de referência precisará ter o conhecimento do procedimento que foi solicitado para você anteriormente em outro plano. 
                                            Portanto, você deverá agendar uma consulta para que o seu médico de referência verifique a conduta.</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-7">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH7" aria-expanded="false" aria-controls="collapseH7">
                                        <h5>Como consultar a Situação de Autorização de exames e procedimentos?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH7" class="collapse" aria-labelledby="how-7" data-parent="#violin">
                                    <div class="card-body">
                                        <p>Você pode acessar o App do Beneficiário, na opção “Autorizações”, acessar o site ou ligar na Central de Atendimento 3220-6200.</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-8">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH8" aria-expanded="false" aria-controls="collapseH8">
                                        <h5>Qual a cobertura do Unimed Pleno?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH8" class="collapse" aria-labelledby="how-8" data-parent="#violin">
                                    <div class="card-body">
                                        <p>A cobertura é ambulatorial, hospitalar com obstetrícia. Essa cobertura é a mesma dos produtos da Unimed Natal e está em conformidade com Rol de procedimentos da ANS.</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-9">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH0" aria-expanded="false" aria-controls="collapseH0">
                                        <h5>Qual a abrangência do plano?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH0" class="collapse" aria-labelledby="how-9" data-parent="#violin">
                                    <div class="card-body">
                                        <p>Grupo de municípios composto por Natal, Parnamirim, Macaíba, Extremoz, Ceará Mirim, São José de Mipibu e São Gonçalo do Amarante. </p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-10">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH10" aria-expanded="false" aria-controls="collapseH10">
                                        <h5>Qual a acomodação disponível?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH10" class="collapse" aria-labelledby="how-10" data-parent="#violin">
                                    <div class="card-body">
                                        <p>Enfermaria e Apartamento.</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-10">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH10" aria-expanded="false" aria-controls="collapseH10">
                                        <h5>Como entrar em contato com a Unidade de Atenção Primária à Saúde?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH10" class="collapse" aria-labelledby="how-10" data-parent="#violin">
                                    <div class="card-body">
                                        <p>Pelo telefone 3220-6273. Ramal 2588</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-11">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH11" aria-expanded="false" aria-controls="collapseH11">
                                        <h5>Qual o horário de atendimento da Unidade de Atenção Primária à Saúde?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH11" class="collapse" aria-labelledby="how-11" data-parent="#violin">
                                    <div class="card-body">
                                        <p>A unidade funciona de segunda-feira a sexta-feria das 08h às 20h e sábado de 08h às 14h.</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-12">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH12" aria-expanded="false" aria-controls="collapseH12">
                                        <h5>Será cobrada coparticipação para as consultas realizadas na Unidade de Atenção Primária à Saúde? E para os procedimentos?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH12" class="collapse" aria-labelledby="how-12" data-parent="#violin">
                                    <div class="card-body">
                                        <p>O pagamento de coparticipação será apenas para consultas e procedimentos realizados fora da Unidade de Atenção Primária à Saúde, 
                                            como consultas com especialistas, exames e procedimentos em outros serviços e consultas de urgência e emergência em pronto-socorro.</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-13">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH13" aria-expanded="false" aria-controls="collapseH13">
                                        <h5>Quando o médico de referência não estiver, o usuário pode consultar com outro médico na Unidade de Atenção Primária à Saúde?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH13" class="collapse" aria-labelledby="how-13" data-parent="#violin">
                                    <div class="card-body">
                                        <p>O ideal é que o paciente seja atendido pelo médico de referência, porém na ausência desse profissional, 
                                            os outros médicos poderão dar os devidos direcionamentos.</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-14">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH14" aria-expanded="false" aria-controls="collapseH14">
                                        <h5>Haverá cobrança de coparticipação em caso de urgência e emergência?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH14" class="collapse" aria-labelledby="how-14" data-parent="#violin">
                                    <div class="card-body">
                                        <p>Sim, pois a isenção da coparticipação será apenas nos serviços ofertados dentro da Unidade de Atenção Primária à Saúde.</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-15">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH15" aria-expanded="false" aria-controls="collapseH15">
                                        <h5>Como agendar uma consulta com o médico de referência?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH15" class="collapse" aria-labelledby="how-15" data-parent="#violin">
                                    <div class="card-body">
                                        <p>O paciente deverá ligar para a Unidade APS através do ramal 2588 ou pelo número 3220-6273</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-16">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH16" aria-expanded="false" aria-controls="collapseH16">
                                        <h5>Estou sentindo uma dor lombar e sei que preciso de um ortopedista, posso ir direto ao especialista?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH16" class="collapse" aria-labelledby="how-16" data-parent="#violin">
                                    <div class="card-body">
                                        <p>Não, o primeiro contato deverá ser com o médico de referência para uma avaliação inicial. 
                                            Após essa avaliação o médico dará os direcionamentos necessários.</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-0" id="how-17">
                                    <a href="#" class="w-100 d-flex align-items-center" data-toggle="collapse" data-target="#collapseH17" aria-expanded="false" aria-controls="collapseH17">
                                        <h5>Já me consulto há muitos anos no meu especialista, vou poder continuar me consultando com ele?</h5>
                                        <span class="btn btn-link">
                                            <img src="/images/adicao-red.svg" width="15px">
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseH17" class="collapse" aria-labelledby="how-17" data-parent="#violin">
                                    <div class="card-body">
                                        <p>Após a consulta com o médico de família, será avaliada a necessidade de acompanhamento compartilhado com o médico especialista, 
                                            a depender da condição de saúde de cada usuário.</p>
                                    </div>											
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>