<section id="what-is">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-5 pt-5 pt-lg-0 text-center">
                <img class="img-fluid pt-5 pt-5" src="images/jumping-g.png">
            </div>
            <div class="text-box col-12 col-md-7 d-flex flex-column pt-5">
                <h1 class="mt-5 pt-lg-5 mb-lg-5 pb-lg-5 pb-3">O QUE É O <b>UNIMED PLENO?</b></h1>
                <p>É um novo modelo de cuidado integral em saúde que tem como objetivo promover a qualidade de vida das pessoas através do acompanhamento 
                    contínuo por uma equipe multidisciplinar, intervindo nos fatores que colocam a sua saúde em risco e trabalhando em conjunto na prevenção 
                    ao aparecimento de novas doenças.</p>
                    
                <h4><strong>O que é Atenção Primária à Saúde?</strong></h4>
                <p>A Atenção Primária é o primeiro nível de atenção em saúde e se caracteriza por um conjunto de ações de saúde, no âmbito
                    individual e coletivo, que abrange a promoção e a proteção da saúde, a prevenção de agravos, o diagnóstico, o tratamento, 
                    a reabilitação, a redução de danos e a manutenção da saúde com o objetivo de desenvolver uma atenção integral que impacte
                    positivamente na situação de saúde das coletividades.
                </p>
                <p>Como exemplo, o modelo de atenção à saúde utilizado no Canadá que figura entre os países de melhor performance de saúde do mundo,
                    tendo modo de organização dos serviços com ênfase na integração, na continuidade do cuidado e na atenção primária, impactando 
                    diretamente na diminuição do número de agravos e internações.
                </p>

                <h4><strong>O que é Promoção da Saúde e Prevenção de Doenças?</strong></h4>
                <p>São ações de educação e comunicação em saúde dirigidas ao incentivo de mudanças comportamentais, hábitos e estilos de vida 
                    de indivíduos, mediante a participação das equipes em ações intersetoriais voltadas para intervenções sobre determinantes 
                    sociais que interferem na qualidade de vida da população.
                </p>
            </div>
        </div>
        <div class="row d-flex justify-content-center">
            <img class="tri-image" src="images/tri-3.png">
        </div>
    </div>
</section>