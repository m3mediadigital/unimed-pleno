<section id="atendimento">
    <div class="container">
        <div class="row">
            <div class="text-box p-5 col-12 text-left">
                <h1 class="text-uppercase pb-2 pt-5">Atendimento</h1>
                <p>Você e sua família serão vinculados a um médico de uma das equipes da atendimento, de acordo com seu endereço informado na contratação do plano. São clínicas aconchegantes, modernas, de fácil acesso e com horários extendidos. Mais uma maneira de você ficar perto do seu médico.</p>
            </div>
            <div class="col-10 mb-5">
                <ul class="nav nav-pills nav-fill justify-content-center mb-4" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a id="pills-duvidas-tab" class="nav-link text-uppercase" data-toggle="pill" href="#pills-duvidas"><strong>Detalhes do Atendimento</strong></a>
                    </li>
                    <li class="nav-item">
                        <a id="pills-unidades-tab" class="nav-link text-uppercase active" data-toggle="pill" href="#pills-unidades"><strong>Unidades de Atendimento</strong></a>
                    </li>
                    <li class="nav-item">
                        <a id="pills-equipe-tab" class="nav-link text-uppercase " data-toggle="pill" href="#pills-equipe"><strong>Equipe</strong></a>
                    </li>
                    
                </ul>
                
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade" id="pills-duvidas" role="tabpanel" aria-labelledby="pills-duvidas-tab">
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header" id="doubt-1">
                                    <h5><strong>Como será seu atendimento?</strong></h5>
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseDOne" aria-expanded="false" aria-controls="collapseDOne">
                                        <i class="fa fa-plus-circle"></i>
                                    </button>
                                </div>

                                <div id="collapseDOne" class="collapse" aria-labelledby="doubt-1" data-parent="#accordion">
                                    <div class="card-body">
                                    <p>Os pacientes  terão um plano personalizado de cuidado, onde a equipe passa a acompanhar todo o histórico de saúde, 
                                        interferências da genética familiar, estilo de vida, meio social, familiar e de trabalho. Garantindo uma melhor 
                                        estratégia de promoção à saúde e prevenção de doenças.</p>

                                    </div>											
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header" id="doubt-2">
                                    <h5><strong>O que é um Médico de Família?</strong></h5>
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseDTwo" aria-expanded="false" aria-controls="collapseDTwo">
                                        <i class="fa fa-plus-circle"></i>
                                    </button>
                                </div>

                                <div id="collapseDTwo" class="collapse" aria-labelledby="doubt-2" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>O Médico de Família e Comunidade (MFC) é um médico que se especializou em cuidar das pessoas independente da idade, 
                                           sexo ou tipo de problema que elas tenham. Ou seja, não são especialistas em órgãos isolados ou doenças, mas especialistas 
                                           em gente. Na Medicina de Família e Comunidade a pessoa é vista como um todo na sua integralidade. Sentimentos e 
                                           expectativas e os contextos familiar e social são considerados no cuidado, para que as recomendações dos médicos 
                                           sejam personalizadas e efetivas.</p>
                                        <p>Outra característica do MFC é o acompanhamento das pessoas ao longo das suas vidas e não apenas em uma consulta pontual. 
                                            Assim, realizam atendimentos desde da concepção até os últimos instantes da vida dos seus pacientes e não conhecem somente 
                                            as doenças, mas conhecem também suas casas, suas famílias e comunidade. Os MFC podem atender em ambulatórios, clínicas e 
                                            hospitais, mas também fazem visitas domiciliar e atividades comunitárias. Por isso, a maioria dos problemas que chegam aos 
                                            médicos de família em comunidade costumam ser resolvidos ali, na Atenção Primária, em conjunto com a equipe multiprofissional.</p>
                                    </div>											
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header" id="doubt-2">
                                    <h5><strong>O que é equipe multiprofissional?</strong></h5>
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseDThree" aria-expanded="false" aria-controls="collapseDThree">
                                        <i class="fa fa-plus-circle"></i>
                                    </button>
                                </div>

                                <div id="collapseDThree" class="collapse" aria-labelledby="doubt-2" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>É uma equipe formada por profissionais de diferentes áreas de atuação na saúde, médicos, enfermeiros, técnicos de 
                                            enfermagem, nutricionistas, psicólogos, educadores físicos, fisioterapeutas e outros, promovendo um tratamento diferenciado, 
                                            enxergando o paciente como um todo e proporcionando um atendimento humanizado. Assim, o quadro clínico é visto de uma 
                                            forma mais ampla, possibilitando que o cuidado  e a prevenção de agravos sejam verdadeiramente resolutivos.</p>   
                                    </div>											
                                </div>
                            </div>

                            <!-- <div class="card">
                                <div class="card-header" id="doubt-2">
                                    <h5><strong>Quais serviços a Unidade [NOME DA UNIDADE] oferece?</strong></h5>
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseDFour" aria-expanded="false" aria-controls="collapseDFour">
                                        <i class="fa fa-plus-circle"></i>
                                    </button>
                                </div>

                                <div id="collapseDFour" class="collapse" aria-labelledby="doubt-2" data-parent="#accordion">
                                    <div class="card-body">
                                        <ul>
                                            <li>Atendimento médico: consultas eletivas baseadas na integração médico-paciente
                                                <ul>
                                                    <li>Retirada de cerúmen</li>
                                                    <li>Coleta de material cérvico-vaginal (preventivo)</li>
                                                    <li>Consulta puerperal e puericultura</li>
                                                    <li>Iremos adicionar mais procedimentos quando a Unidade definitiva for entregue</li>
                                                </ul>
                                            </li>
                                            <li>Atendimento de enfermagem: consultas de enfermagem e procedimentos em geral
                                                <ul>
                                                    <li>Coleta de material cérvico-vaginal (preventivo)</li>
                                                    <li>Orientação da gestante sobre o autocuidado, cuidados com a criança e amamentação</li>
                                                    <li>Orientação sobre os cuidados relacionados às doenças crônicas (diabetes, hipertensão, etc.)</li>
                                                    <li>Iremos adicionar mais procedimentos quando a Unidade definitiva for entregue</li>
                                                </ul>
                                            </li>
                                            <li>Atendimento de  psicologia: consultas e sessões de psicologia</li>
                                        </ul>
                                    </div>											
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="tab-pane fade show active" id="pills-unidades" role="tabpanel" aria-labelledby="pills-unidades-tab">
                        <div class="col-12">
                            <ul class="nav nav-pills nav-fill mb-3" id="maps-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase active" id="mapa-1" data-toggle="pill" href="#pills-mapa-1" role="tab" aria-controls="pills-mapa-1" aria-selected="true">Unidade 1</a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a class="nav-link text-uppercase" id="mapa-2" data-toggle="pill" href="#pills-mapa-2" role="tab" aria-controls="pills-mapa-2" aria-selected="false">Unidade 2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" id="mapa-3" data-toggle="pill" href="#pills-mapa-3" role="tab" aria-controls="pills-mapa-3" aria-selected="true">Unidade 3</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" id="mapa-4" data-toggle="pill" href="#pills-mapa-4" role="tab" aria-controls="pills-mapa-4" aria-selected="false">Unidade 4</a>
                                </li> -->
                            </ul>
                            <div class="tab-content" id="pills-unidadesContent">
                                <div class="tab-pane fade show active" id="pills-mapa-1" role="tab-panel" aria-labelledby="pills-mapa-1">
                                    <div class="row">
                                        <div class="col-12 col-md-5">
                                            <address class="pt-5">
                                                Av Prudente de Morais, N° 1228 Candelária, Natal. (CP: 59020-145).<br><br>
                                                <!--
                                                Horário de Funcionamento:<br>
                                                Segunda a quinta-feira: de 8h às 18h<br>
                                                Sexta-feira: 8h às 17h<br>
                                                -->
                                            </address>
                                        </div>
                                        <div class="col-12 col-md-7">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3969.4345007942043!2d-35.20370145443795!3d-5.7941392184794!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7b30011d5c04d39%3A0xa5b7bdd2b323aeee!2sAv.%20Prudente%20de%20Morais%2C%201228%20-%20Candel%C3%A1ria%2C%20Natal%20-%20RN%2C%2059020-145!5e0!3m2!1spt-BR!2sbr!4v1626094420729!5m2!1spt-BR!2sbr" width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="tab-pane fade" id="pills-mapa-2" role="tab-panel" aria-labelledby="pills-mapa-2">
                                    <div class="row">
                                        <div class="col-12 col-md-5">
                                            <address class="pt-5">
                                                R. Apodi, 228 - Cidade Alta, Natal - RN, 59025-170<br><br>
                                                Horário de Funcionamento:<br>
                                                Segunda à sexta: 8h às 19h<br>
                                                Sábado: 8h às 16h<br>
                                                Domingo: 8h às 13h<br>
                                            </address>
                                        </div>
                                        <div class="col-12 col-md-7">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3969.490312053534!2d-35.2058171847072!3d-5.7861947958069795!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7b3000d4218aae9%3A0xeed21e297e8ee559!2sUnimed!5e0!3m2!1spt-BR!2sbr!4v1603801906214!5m2!1spt-BR!2sbr" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-mapa-3" role="tab-panel" aria-labelledby="pills-mapa-3">
                                    <div class="row">
                                        <div class="col-12 col-md-5">
                                            <address class="pt-5">
                                                R. Apodi, 228 - Cidade Alta, Natal - RN, 59025-170<br><br>
                                                Horário de Funcionamento:<br>
                                                Segunda à sexta: 8h às 19h<br>
                                                Sábado: 8h às 16h<br>
                                                Domingo: 8h às 13h<br>
                                            </address>
                                        </div>
                                        <div class="col-12 col-md-7">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3969.4660507261433!2d-35.20861318470713!3d-5.789649595804487!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7b30013e8382449%3A0x79beb181c53a2815!2sUnimed%20Natal%20-%20Central%20de%20Atendimento%20ao%20Cliente!5e0!3m2!1spt-BR!2sbr!4v1603802207050!5m2!1spt-BR!2sbr" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-mapa-4" role="tab-panel" aria-labelledby="pills-mapa-4">
                                    <div class="row">
                                        <div class="col-12 col-md-5">
                                            <address class="pt-5">
                                                R. Apodi, 228 - Cidade Alta, Natal - RN, 59025-170<br><br>
                                                Horário de Funcionamento:<br>
                                                Segunda à sexta: 8h às 19h<br>
                                                Sábado: 8h às 16h<br>
                                                Domingo: 8h às 13h<br>
                                            </address>
                                        </div>
                                        <div class="col-12 col-md-7">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3969.4741225831854!2d-35.20293248470726!3d-5.7885003958053165!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7b30013242ab9c3%3A0x4791269ba6e01c02!2sUnimed%20Federa%C3%A7%C3%A3o%20do%20Estado%20do%20Rio%20Grande%20do%20Norte!5e0!3m2!1spt-BR!2sbr!4v1603802831556!5m2!1spt-BR!2sbr" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-equipe" role="tabpanel" aria-labelledby="pills-equipe-tab">
                        <div class="col-12">
                            <div class="tab-content" id="pills-equipeContent">
                                <div class="tab-pane fade show active" id="pills-mapa-1" role="tab-panel" aria-labelledby="pills-mapa-1">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <address class="pt-5">
                                            <p>Synara Simonica: Auxiliar Administrativo</p>
                                            <p>Cecília Brenda: Assistente Administrativo</p>
                                            <p>Deyvisson Ribeiro: Técnico de Enfermagem</p>
                                            <p>Mônica de Oliveira: Enfermeira</p>
                                            <p>Neuma Marinho: Médica de Família</p>
                                            <p>Nancy Cristina: Médica de Família</p>
                                            <p>Cristiane Vasconcelos: Psicóloga</p>
                                            <p>Camilla Cavalcanti: Psicóloga</p>
                                            </address>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>