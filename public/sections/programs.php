<section id="programs" class="hazy-bg">
    <img class="img-fluid w-100" src="images/heian.png">
    <div class="container pb-5 mb-5">
        <div class="row pb-5 mb-5">
            <div class="text-box pt-5 pb-5 col-12 text-left">
                <h1 class="text-uppercase pt-5 pb-5">Programas</h1>
                <p>Os programas desenvolvidos na Atenção Primária à Saúde visam proporcionar qualidade de vida, bem-estar, promoção e prevenção à saúde. Tais programas buscam disseminar boas práticas como forma de conciliar as demandas do dia a dia de acordo com as necessidades individuais da saúde, corpo e mente</p>
            </div>
            
            <div class="col-12 col-lg-6 pb-3">
                <a class="btn btn-success w-100 text-uppercase border-0" data-toggle="collapse" href="#collapseUm" role="button" aria-expanded="false" aria-controls="collapseUm">
                    <div class="row">
                        <div class="col-5">
                            <svg xmlns="http://www.w3.org/2000/svg" width="46.38" height="47.734" viewBox="0 0 46.38 47.734">
                                <g id="cigarro-eletronico" transform="translate(-1 -1)">
                                    <path id="Caminho_71" data-name="Caminho 71" d="M36.771,28.007h.86v-1.54h-.86a2.307,2.307,0,0,1-2.191-1.579l-.068-.2-1.461.487.068.2a3.844,3.844,0,0,0,3.652,2.632Z" transform="translate(-7.375 -5.449)"/>
                                    <path id="Caminho_72" data-name="Caminho 72" d="M48.475,19a5.894,5.894,0,0,0-1.9-8.716V8.754A6.208,6.208,0,0,0,36.559,3.845l-.153-.159A8.829,8.829,0,0,0,30.083,1H30A9,9,0,0,0,23.59,3.648L23.1,4.14,22.81,4.2A4.914,4.914,0,0,0,20.7,12.854l1.272,1.017a4.616,4.616,0,0,0,1.65,3.013,26.735,26.735,0,0,0-1.643,5.346,2.805,2.805,0,0,0-.044.507,2.951,2.951,0,0,0,1.54,2.6v2.287l-3.85,3.85-1.221-1.221,1.765-1.765L19.084,27.4l-1.8,1.8a2.7,2.7,0,0,0-3.365.285L6.314,37.093a4.487,4.487,0,0,0,0,6.346L9.3,46.425a2.628,2.628,0,0,0,3.717,0l.836-.836L17,48.734H45.035V42.575A4.627,4.627,0,0,0,41.4,38.063l-2.521-2.016v-1.52l7.882-5.067a6.3,6.3,0,0,0,1.13-9.679ZM23.5,22.5a24.9,24.9,0,0,1,1.474-4.848,4.606,4.606,0,0,0,6.205-4.333h1.492a3.831,3.831,0,0,1,3.574,2.42l2.112,5.279h.861a2.741,2.741,0,0,1,1.52,5.021l-3.4,2.266V35.98l-.175.1a13.144,13.144,0,0,1-5.985,1.845V34.876h3.08v-1.54H28.867a3.854,3.854,0,0,1-3.85-3.85v-5.1l-.75-.375a1.421,1.421,0,0,1-.79-1.278,1.309,1.309,0,0,1,.02-.239Zm-1.56,19.757v-.451a3.07,3.07,0,0,0-1.611-2.69L21.938,37.5a2.631,2.631,0,0,0,.566-2.876l2-2a5.382,5.382,0,0,0,4.367,2.244h.77V38.25l-.225.112a10.672,10.672,0,0,0-5.323,6.046Zm-3.08-1.991a1.541,1.541,0,0,1,1.54,1.54v.451l-3.85,3.85-3.888-3.888a2.5,2.5,0,1,1,3.531-3.531l1.578,1.578ZM23.493,29.79A5.332,5.332,0,0,0,23.76,31.2L21.553,33.4l-.836-.836Zm-6.945.785,4.3,4.3a1.089,1.089,0,0,1,0,1.54l-.451.451-5.841-5.841.451-.451A1.128,1.128,0,0,1,16.549,30.575ZM11.929,45.336a1.128,1.128,0,0,1-1.54,0L7.4,42.35a2.948,2.948,0,0,1,0-4.168l6.066-6.066,5.841,5.841-.77.77h-.132L17.28,37.6a4.036,4.036,0,1,0-5.708,5.708L12.765,44.5Zm5.708,1.859,3.531-3.531L24.7,47.195Zm18.159,0h-8.92l-1.548-1.548a9.129,9.129,0,0,1,4.539-5.774,6.425,6.425,0,0,1,2,3.273l.362,1.446,3.58-2.387c-.009.122-.018.246-.018.37Zm7.7-4.619v4.619H37.336V42.575a3.08,3.08,0,0,1,6.159,0Zm-4.44-4.415a4.634,4.634,0,0,0-2.239,1.526l-3.644,2.429a7.967,7.967,0,0,0-1.583-2.673,14.676,14.676,0,0,0,6.364-2.037l.089-.054Zm6.87-10L38.876,32.7V29.129l2.713-1.809a4.28,4.28,0,0,0-2.19-7.838l-1.725-4.315a5.364,5.364,0,0,0-5-3.388h-1.92a3.07,3.07,0,0,0-5.731,1.54h1.54a1.54,1.54,0,0,1,3.08,0,3.08,3.08,0,0,1-6.159,0v-.3A4.407,4.407,0,0,1,25.445,9.34l-.854-1.281a5.934,5.934,0,0,0-2.547,3.9l-.38-.3a3.375,3.375,0,0,1,1.446-5.944l.747-.149.821-.822A7.468,7.468,0,0,1,30,2.54h.087A7.285,7.285,0,0,1,35.3,4.755l.135.139-.95.95,1.089,1.089,1.485-1.485a4.674,4.674,0,0,1,7.979,3.306V9.7c-.147-.033-.295-.063-.447-.085L43.6,9.477,43.387,11l.985.141a4.361,4.361,0,0,1,2.872,6.933l-.554.738c-.034-.021-.063-.047-.1-.068l-1.161-.7-.792,1.32,1.161.7a4.768,4.768,0,0,1,.125,8.1Z" transform="translate(-2.275)"/>
                                    <path id="Caminho_73" data-name="Caminho 73" d="M8.7,27.319h6.159v-1.54H8.7A6.166,6.166,0,0,1,2.54,19.619a3.08,3.08,0,1,1,6.159,0,1.54,1.54,0,0,1-3.08,0H4.08a3.08,3.08,0,1,0,6.159,0,4.619,4.619,0,1,0-9.239,0A7.708,7.708,0,0,0,8.7,27.319Z" transform="translate(0 -3.221)"/>
                                    <path id="Caminho_74" data-name="Caminho 74" d="M7.159,38.389a2.313,2.313,0,0,0-2.31-2.31H4.08v1.54h.77a.77.77,0,0,1,0,1.54,2.31,2.31,0,1,1,0-4.619h8.469V33H4.85a3.85,3.85,0,0,0,0,7.7A2.313,2.313,0,0,0,7.159,38.389Z" transform="translate(0 -7.363)"/>
                                </g>
                            </svg>
                        </div>
                        <div class="col-7 d-flex align-items-center">
                            TERAPIA COMUNITÁRIA
                        </div>
                    </div>
                </a>
                <div class="collapse " id="collapseUm" style="color: #5b5c65;border-radius: 10px;padding: 20px;background-color: #e0e0e0;border: 0px;">
                    <div class="collapse-body">
                        <h5>O QUE É?</h5> 
                        <p class="text-justify">Terapia Comunitária (TCI) é definida como uma metodologia de intervenção em comunidades, por meio de encontros interpessoais e intercomunitários. 
                        Seu objetivo é a promoção da saúde mental através da construção de vínculos solidários, valorização das experiências de vida dos participantes, 
                        do resgate da identidade, da restauração da autoestima e ampliação da percepção dos problemas e possibilidades de resolução a partir das competências locais.</p>

                        <h5>COMO PARTICIPAR?</h5>
                        <p>Inscreva-se <a href ="https://apps.unimednatal.com.br/sgpgnet/solicitacoes/grupo/terapia">aqui</a> ou pelo telefone 3220-6273.
                        As reuniões do próximo grupo ainda serão definidas.</p>

                        <h5>PÚBLICO-ALVO</h5>
                        <p>Exclusivo para clientes da Unimed Natal.</p>
                    </div>
                </div>
            </div>
                
            <div class="col-12 col-lg-6 pb-3">
                <a class="btn btn-success w-100 text-uppercase border-0" data-toggle="collapse" href="#collapseDois" role="button" aria-expanded="false" aria-controls="collapseDois">
                    <div class="row">
                        <div class="col-5">
                            <svg xmlns="http://www.w3.org/2000/svg" width="46.38" height="47.734" viewBox="0 0 46.38 47.734">
                                <g id="cigarro-eletronico" transform="translate(-1 -1)">
                                    <path id="Caminho_71" data-name="Caminho 71" d="M36.771,28.007h.86v-1.54h-.86a2.307,2.307,0,0,1-2.191-1.579l-.068-.2-1.461.487.068.2a3.844,3.844,0,0,0,3.652,2.632Z" transform="translate(-7.375 -5.449)"/>
                                    <path id="Caminho_72" data-name="Caminho 72" d="M48.475,19a5.894,5.894,0,0,0-1.9-8.716V8.754A6.208,6.208,0,0,0,36.559,3.845l-.153-.159A8.829,8.829,0,0,0,30.083,1H30A9,9,0,0,0,23.59,3.648L23.1,4.14,22.81,4.2A4.914,4.914,0,0,0,20.7,12.854l1.272,1.017a4.616,4.616,0,0,0,1.65,3.013,26.735,26.735,0,0,0-1.643,5.346,2.805,2.805,0,0,0-.044.507,2.951,2.951,0,0,0,1.54,2.6v2.287l-3.85,3.85-1.221-1.221,1.765-1.765L19.084,27.4l-1.8,1.8a2.7,2.7,0,0,0-3.365.285L6.314,37.093a4.487,4.487,0,0,0,0,6.346L9.3,46.425a2.628,2.628,0,0,0,3.717,0l.836-.836L17,48.734H45.035V42.575A4.627,4.627,0,0,0,41.4,38.063l-2.521-2.016v-1.52l7.882-5.067a6.3,6.3,0,0,0,1.13-9.679ZM23.5,22.5a24.9,24.9,0,0,1,1.474-4.848,4.606,4.606,0,0,0,6.205-4.333h1.492a3.831,3.831,0,0,1,3.574,2.42l2.112,5.279h.861a2.741,2.741,0,0,1,1.52,5.021l-3.4,2.266V35.98l-.175.1a13.144,13.144,0,0,1-5.985,1.845V34.876h3.08v-1.54H28.867a3.854,3.854,0,0,1-3.85-3.85v-5.1l-.75-.375a1.421,1.421,0,0,1-.79-1.278,1.309,1.309,0,0,1,.02-.239Zm-1.56,19.757v-.451a3.07,3.07,0,0,0-1.611-2.69L21.938,37.5a2.631,2.631,0,0,0,.566-2.876l2-2a5.382,5.382,0,0,0,4.367,2.244h.77V38.25l-.225.112a10.672,10.672,0,0,0-5.323,6.046Zm-3.08-1.991a1.541,1.541,0,0,1,1.54,1.54v.451l-3.85,3.85-3.888-3.888a2.5,2.5,0,1,1,3.531-3.531l1.578,1.578ZM23.493,29.79A5.332,5.332,0,0,0,23.76,31.2L21.553,33.4l-.836-.836Zm-6.945.785,4.3,4.3a1.089,1.089,0,0,1,0,1.54l-.451.451-5.841-5.841.451-.451A1.128,1.128,0,0,1,16.549,30.575ZM11.929,45.336a1.128,1.128,0,0,1-1.54,0L7.4,42.35a2.948,2.948,0,0,1,0-4.168l6.066-6.066,5.841,5.841-.77.77h-.132L17.28,37.6a4.036,4.036,0,1,0-5.708,5.708L12.765,44.5Zm5.708,1.859,3.531-3.531L24.7,47.195Zm18.159,0h-8.92l-1.548-1.548a9.129,9.129,0,0,1,4.539-5.774,6.425,6.425,0,0,1,2,3.273l.362,1.446,3.58-2.387c-.009.122-.018.246-.018.37Zm7.7-4.619v4.619H37.336V42.575a3.08,3.08,0,0,1,6.159,0Zm-4.44-4.415a4.634,4.634,0,0,0-2.239,1.526l-3.644,2.429a7.967,7.967,0,0,0-1.583-2.673,14.676,14.676,0,0,0,6.364-2.037l.089-.054Zm6.87-10L38.876,32.7V29.129l2.713-1.809a4.28,4.28,0,0,0-2.19-7.838l-1.725-4.315a5.364,5.364,0,0,0-5-3.388h-1.92a3.07,3.07,0,0,0-5.731,1.54h1.54a1.54,1.54,0,0,1,3.08,0,3.08,3.08,0,0,1-6.159,0v-.3A4.407,4.407,0,0,1,25.445,9.34l-.854-1.281a5.934,5.934,0,0,0-2.547,3.9l-.38-.3a3.375,3.375,0,0,1,1.446-5.944l.747-.149.821-.822A7.468,7.468,0,0,1,30,2.54h.087A7.285,7.285,0,0,1,35.3,4.755l.135.139-.95.95,1.089,1.089,1.485-1.485a4.674,4.674,0,0,1,7.979,3.306V9.7c-.147-.033-.295-.063-.447-.085L43.6,9.477,43.387,11l.985.141a4.361,4.361,0,0,1,2.872,6.933l-.554.738c-.034-.021-.063-.047-.1-.068l-1.161-.7-.792,1.32,1.161.7a4.768,4.768,0,0,1,.125,8.1Z" transform="translate(-2.275)"/>
                                    <path id="Caminho_73" data-name="Caminho 73" d="M8.7,27.319h6.159v-1.54H8.7A6.166,6.166,0,0,1,2.54,19.619a3.08,3.08,0,1,1,6.159,0,1.54,1.54,0,0,1-3.08,0H4.08a3.08,3.08,0,1,0,6.159,0,4.619,4.619,0,1,0-9.239,0A7.708,7.708,0,0,0,8.7,27.319Z" transform="translate(0 -3.221)"/>
                                    <path id="Caminho_74" data-name="Caminho 74" d="M7.159,38.389a2.313,2.313,0,0,0-2.31-2.31H4.08v1.54h.77a.77.77,0,0,1,0,1.54,2.31,2.31,0,1,1,0-4.619h8.469V33H4.85a3.85,3.85,0,0,0,0,7.7A2.313,2.313,0,0,0,7.159,38.389Z" transform="translate(0 -7.363)"/>
                                </g>
                            </svg>
                        </div>
                        <div class="col-7 d-flex align-items-center">
                            ATENÇÃO AO TABAGISTA
                        </div>
                    </div>
                </a>
                <div class="collapse" id="collapseDois" style="color: #5b5c65;border-radius: 10px;padding: 20px;background-color: #e0e0e0;border: 0px;">
                    <div class="collapse-body">
                        <h5>O QUE É?</h5>
                        <p class="text-justify">Grupo de educação em saúde da Unimed Natal criado para ajudar os clientes fumantes a deixar o cigarro e conquistar mais qualidade de vida.</p>
                        <p class="text-justify">Os participantes do grupo recebem orientações importantes para largar o cigarro e, assim, prevenir riscos de doenças relacionadas ao tabagismo. 
                            As atividades são realizadas na Unidade de Atenção Primária à Saúde. Ao todo, os integrantes participam de quatro encontros estruturados, 
                            realizados semanalmente. O trabalho é conduzido por uma equipe interdisciplinar, formada por psicóloga, enfermeira e médica.</p>
                        
                        <h5>COMO PARTICIPAR?</h5>
                        <p>Inscreva-se <a href="https://apps.unimednatal.com.br/sgpgnet/solicitacoes/grupo/tabagista">aqui</a> ou pelo telefone 3220-6273.
                        As reuniões do próximo grupo ainda serão definidas.</p>
                        
                        <h5>PÚBLICO-ALVO</h5>
                        <p>Exclusivo para clientes da Unimed Natal.</p>
                    </div>
                </div>
            </div>

        </div>
        <div class="d-flex justify-content-center">
            <img class="tri-image position-absolute" src="images/tri-6.png">
        </div>
    </div>
</section>