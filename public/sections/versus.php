<section id="versus">
    <div class="box-text green-bg pt-5 pb-5">
        <div class="container pb-5">
            <div class="row pb-5">
                <div class="text-box col-12 text-left pb-5">
                    <h1 class="text-uppercase pt-5 pb-5 text-center">
                        Como é a utilização no modelo antigo 
                        <br>x
                        <br>Como será a utilização com o Unimed Pleno
                    </h1>		
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <div class="card pt-5 pb-5 border-0">
                    <h5 class="text-uppercase text-center pb-5"><strong>Como é hoje</strong></h5>
                    <ul class="pb-2 pr-3">
                        <li>
                            <p class="mb-0">O médico que atende você não possui seu histórico de saúde;</p>
                        </li>
                        <li>
                            <p class="mb-0">O paciente é quem escolhe em qual especialista irá se consultar, aumentando as chances de erro;</p>
                        </li>
                        <li>
                            <p class="mb-0">Tratamento voltado para a doença, sem acompanhamento;</p>
                        </li>
                        <li>
                            <p class="mb-0">Não possui uma equipe multidisciplinar à disposição.</p>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="item">
                <div class="card pt-5 pb-5 border-0">
                    <h5 class="text-uppercase text-center pb-5"><strong>Como é com o Unimed Pleno</strong></h5>
                    <ul class="pb-2 pr-2 ">
                        <li>
                            <p class="mb-0">O médico de referência que atende você o conhece, sabe seu histórico;</p>
                        </li>
                        <li>
                            <p class="mb-0">O seu médico de referência indicará o especialista que irá te consultar e ambos mantêm contato para melhor solução do seu problema;</p>
                        </li>
                        <li>
                            <p class="mb-0">Tratamento voltado para a pessoa, com acompanhamento e continuidade;</p>
                        </li>
                        <li>
                            <p class="mb-0">Possui uma equipe multidisciplinar à disposição para prover os cuidados necessários de forma integral;</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-center pt-5 pb-5">
            <img class="tri-image" width="41px" src="images/tri-4.png">
        </div>
    </div>
</section>