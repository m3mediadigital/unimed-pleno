<div class="menu-todo nav" id="sidebar">
    <div class="menu w-100">
        <ul class="navbar-nav justify-content-center">
            <li class="nav-item d-flex justify-content-center">
                <a class="nav-link w-75 text-center" href="#">
                    <img src="images/unimed-logo.png">
                </a>
            </li>
            <li class="nav-item d-flex justify-content-center">
                <a class="nav-link border-bottom w-75 text-center" href="#what-is">O Unimed PLENO</a>
            </li>
            <li class="nav-item d-flex justify-content-center">
                <a class="nav-link border-bottom w-75 text-center" href="#atendimento">Atendimento</a>
            </li>
            <li class="nav-item d-flex justify-content-center">
                <a class="nav-link border-bottom w-75 text-center" href="#utilize">Como Utilizar</a>
            </li>
            <li class="nav-item d-flex justify-content-center">
                <a class="nav-link border-bottom w-75 text-center" href="#diferentials">Diferenciais</a>
            </li>
            <li class="nav-item d-flex justify-content-center">
                <a class="nav-link border-bottom w-75 text-center" href="#programs">Programas</a>
            </li>
            <li class="nav-item d-flex justify-content-center">
                <a class="nav-link w-75 text-center" href="#faq">Duvidas Frequentes</a>
            </li>
        </ul>
        <ul class="nav justify-content-between nav-footer pt-lg-5 pt-4 pb-3 w-100">
            <li class="nav-item w-100 position-absolute">
                <ul class="nav justify-content-between w-100">
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center" href="#">
                            <img src="/images/ans-logo.svg" alt="Facebook" class="icons">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center" href="#">
                            <img src="/images/ans-number.svg" alt="Instagram" class="icons">
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item d-flex align-items-center justify-content-center w-100 pt-2">
                <span>Desenvolvido por Nova M3</span>
            </li>
        </ul>
    </div>
</div>

<div class="cheeseburger-menu-overlay trasition d-none" style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; z-index: 1001; background: rgba(0, 0, 0, 0.3); 
    opacity: 1; transition: opacity 0.6s ease 0s; transform: none;"></div>