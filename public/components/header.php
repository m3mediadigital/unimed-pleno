<section id="topo">
    <nav class="navbar navbar-expand-lg d-none d-lg-block pt-3">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="images/unimed-logo.png">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#what-is">O Unimed Pleno <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#atendimento">Atendimento</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#utilize">Como Utilizar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#diferentials">Diferenciais</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#programs">Programas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#faq">Duvidas Frequentes</a>
                    </li>
                </ul>
            </div>
       </div>
    </nav>
    <div class="hamburger hamburger--squeeze js-hamburger d-lg-none position-absolute" data-toggle="collapse" data-target="#navbarDesktop" aria-controls="navbarDesktop" aria-expanded="true">
        <div class="hamburger-box">
            <div class="hamburger-inner"></div>
        </div>
    </div>

    <img class="img-fluid w-100" src="images/unimed-pleno-topo.png">
    <div class="container pt-4">
        <div class="d-flex align-items-center h-100 pb-4 pb-lg-0">
            <img src="images/icone-aps.png">
        </div>
        <div class="row d-flex justify-content-center">
            <img class="tri-image" src="images/tri-1.png">
        </div>
    </div>
</section>