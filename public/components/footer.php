<?php

    $contacts = [
        [
            'title' => 'vendas',
            'subtitle' => '(84) 3220-6200',
            'content' => ' '
        ],
        [
            'title' => 'consultas',
            'subtitle' => '(84) 3220-6400',
            'content' => ' '
        ],
         [
            'title' => 'exames',
            'subtitle' => '(84) 3220-1515',
            'content' => ' '
        ],
        [
            'title' => 'hospital',
            'subtitle' => '(84) 3220-1500',
            'content' => ' '
        ],
        [
            'title' => 'central de relacionamento empresarial',
            'subtitle' => '(84) 3220-6303',
            'content' => ' '
        ],
        [
            'title' => 'sac 24h',
            'subtitle' => '0800 084 2323',
            'content' => 'Exclusivo para reclamação,<br>
                            cancelamento, informação e dúvida.'
        ],
        [
            'title' => 'central de atendimento 24h',
            'subtitle' => '(84) 3220-6200',
            'content' => 'Assuntos relacionados à autorização, rede
                            credenciada, fatura, cartão, contratos e demais serviços.'
        ],
        [
            'title' => 'sos',
            'subtitle' => '0800 084 2001',
            'content' => ' '
        ],
    ];

?>

<section id="footer" class="pt-lg-5 hazy-bg">
	<div class="container">
        <div class="d-none d-lg-block">
            <div class="row w-100">
                <div class="col-3 text-center">
                    <ul class="nav nav-icons">
                        <li class="nav-item">
                            <a class="nav-link d-flex align-items-center" href="#">
                                <img src="/images/facebook.svg" alt="Facebook" class="icons">
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex align-items-center" href="#">
                                <img src="/images/instagram.svg" alt="Instagram" class="icons">
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex align-items-center" href="#">
                                <img src="/images/twitter.svg" alt="Twitter" class="icons">
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex align-items-center" href="#">
                                <img src="/images/youtube.svg" alt="Youtube" class="icons">
                            </a>
                        </li>
                    </ul>
                    <p class="text-center pt-2 mb-0">Baixe o nosso app</p>
                    <ul class="nav pl-3 pr-3">
                        <li class="nav-item w-50">
                            <a class="nav-link pl-0" href="#">
                                <img src="/images/apple.svg" alt="Facebook" class="w-100">
                            </a>
                        </li>
                        <li class="nav-item w-50">
                            <a class="nav-link pr-0" href="#">
                                <img src="/images/google.svg" alt="Instagram" class="w-100">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-9">
                    <div class="row">
                        <?php foreach($contacts as $item): ?>
                            <div class="col-3 pb-4">
                                <div class="contacts">
                                    <strong class="text-uppercase"><?= $item['title'] ?></strong>
                                    <p class="mb-0"><?= $item['subtitle'] ?></p>
                                    <small><?= $item['content'] ?></small>
                                </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <ul class="nav justify-content-between nav-footer pt-lg-5 pt-4 pb-3">
            <li class="nav-item">
                <ul class="nav justify-content-between">
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center" href="#">
                            <img src="/images/ans-logo.svg" alt="Facebook" class="icons">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center" href="#">
                            <img src="/images/ans-number.svg" alt="Instagram" class="icons">
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item d-flex align-items-center">
                <span>Desenvolvido por Nova M3</span>
            </li>
        </ul>
    </div>			
</section>
