<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<!-- <link href="/images/favicon.ico" type="image/x-icon" rel="icon" />
    <link href="/images/favicon.ico" type="image/x-icon" rel="shortcut icon" /> -->
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <!-- <meta name="URL" content="http://www.rovemaenergia.com.br" /> -->
    <link rel="stylesheet" href="css/app.css" />


    <title>Unimed Pleno</title>
</head>
<body>
	<div class="body-container">
		<?php 
		  	include "components/menu-mobile.php";
			include 'components/header.php';
			include 'sections/video.php';
			include 'sections/whats_is.php';
			include 'sections/atendimento.php';
			include 'sections/utilize.php';
			include 'sections/versus.php';
			include 'sections/diferentials.php';
			include 'sections/programs.php';
			include 'sections/faq.php';
			include 'components/footer.php';
		?>	
	</div>

	<script src="js/app.js"></script>
    <script src="https://use.fontawesome.com/ac2432330e.js"></script>
</body>
</html>